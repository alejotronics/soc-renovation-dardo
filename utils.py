from bs4 import BeautifulSoup


def print_renew_result(html):
    soup = BeautifulSoup(html, 'html.parser')
    tds = soup.find("div", {"class": "cuadroInformativo"}).find_all("td")
    tds = [td.getText(strip=True) for td in tds]

    # from 1 to 8 every 2 items
    for r in range(1, 9, 2):
        title = tds[r-1]
        if ':' not in tds[r-1]:
            title += ':'
        print("%s %s" % (title, tds[r]))
