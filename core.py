from utils import print_renew_result
import requests

BASE_URL = 'https://www.oficinadetreball.gencat.cat/socfuncions/'
LOGIN_FORM = 'LoginNifCodi.do'
LOGOUT_FORM = 'LogoffDemandants.do'
RENEW_FORM = 'ResultatRenovacioT.do'
DARDO_FORM = 'PeticioDardo.do'
SITUATION_FORM = 'ControlCanviSituacioAdm.do'


class SOC:
    def __init__(self, dni, tarjeta, nie='D'):
        self.payload = {
            'nie': nie,
            'dni': dni,
            'tarjeta': tarjeta,
        }
        self.__is_login = False
        self.session = requests.session()

    def __login(self):
        if not self.__is_login:
            response = self.session.post(
                BASE_URL + LOGIN_FORM, params=self.payload)
            if response.status_code == 200:
                self.__is_login = True
            else:
                print('Error on login - code: %d' % response.status_code)

    def logout(self):
        response = self.session.post(BASE_URL + LOGOUT_FORM)
        if response.status_code != 200:
            print('Error on logout - code: %d' % response.status_code)

    def renovar(self):
        self.__login()
        response = self.session.post(BASE_URL + RENEW_FORM)
        if response.status_code == 200:
            print_renew_result(response.content)
        else:
            print('Error on renew - code: %d' % response.status_code)
